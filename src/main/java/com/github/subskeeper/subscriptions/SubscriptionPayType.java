package com.github.subskeeper.subscriptions;

public enum SubscriptionPayType {
    FREE, TRIAL, PAID
}
