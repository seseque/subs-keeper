package com.github.subskeeper.subscriptions;

public enum SubscriptionPeriodType {
    MONTHLY, ANNUAL
}
