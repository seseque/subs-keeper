package com.github.subskeeper.subscriptions;

public enum SubscriptionGroupType {
    PERSONAL, GROUP
}
