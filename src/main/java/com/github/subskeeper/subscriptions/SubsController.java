package com.github.subskeeper.subscriptions;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/subs")
public class SubsController {
    @GetMapping("/details")
    String getDetails() {
        return "I'm working...";
    }
}
