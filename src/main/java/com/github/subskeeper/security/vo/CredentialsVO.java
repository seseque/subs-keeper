package com.github.subskeeper.security.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class CredentialsVO {
    @Email(message = "Email should be valid")
    public String email;
    @JsonProperty("password")
    @Size(min = 6, max = 20)
    public String rawPassword;

    public CredentialsVO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRawPassword() {
        return rawPassword;
    }

    public void setRawPassword(String rawPassword) {
        this.rawPassword = rawPassword;
    }
}
