package com.github.subskeeper.security.services;

import com.github.subskeeper.user.entity.Authority;
import com.github.subskeeper.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Slf4j
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return createSpringSecurityUser(username);
    }

    private User createSpringSecurityUser(String email) {
        com.github.subskeeper.user.entity.User customUser = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(""));
        return new User(
                email,
                customUser.getPassword(),
                getSpringSecurityAuthority(customUser.getRoles())
        );
    }

    private Collection<SimpleGrantedAuthority> getSpringSecurityAuthority(Set<Authority> authorities) {
        Set<SimpleGrantedAuthority> springSecurityAuthorities = new HashSet();
        for (Authority authority : authorities) {
            springSecurityAuthorities.add(new SimpleGrantedAuthority(authority.getName()));
        }
        return springSecurityAuthorities;
    }
}
