package com.github.subskeeper.security.controllers;

import com.github.subskeeper.security.vo.CredentialsVO;
import com.github.subskeeper.security.vo.TokenVO;
import com.github.subskeeper.user.exceptions.EmailAlreadyUsedException;
import com.github.subskeeper.user.exceptions.InvalidUserException;
import com.github.subskeeper.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    UserService userService;

    @Autowired
    AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity createUser(@RequestBody CredentialsVO request) {
        try {
            String token = userService.createUser(request.email, request.rawPassword);
            return ResponseEntity.status(HttpStatus.CREATED).body(new TokenVO(token));
        } catch (EmailAlreadyUsedException e) {
            return ResponseEntity.ok(e);
        }
    }

    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity authenticate(@RequestBody CredentialsVO auth) {
        try {
            String token = userService.authenticate(auth.email, auth.rawPassword);
            return ResponseEntity.status(HttpStatus.OK).body(new TokenVO(token));
        } catch (InvalidUserException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
