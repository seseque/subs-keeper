package com.github.subskeeper.user.exceptions;

public class InvalidUserException extends RuntimeException {
    String message;

    public InvalidUserException(String message) {
        this.message = message;
    }
}
