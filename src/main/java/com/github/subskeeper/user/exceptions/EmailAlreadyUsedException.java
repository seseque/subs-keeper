package com.github.subskeeper.user.exceptions;

public class EmailAlreadyUsedException extends RuntimeException {
    String message;

    public EmailAlreadyUsedException(String message) {
        this.message = message;
    }
}
