package com.github.subskeeper.user;

import com.github.subskeeper.user.entity.Authority;
import com.github.subskeeper.user.entity.User;
import com.github.subskeeper.user.exceptions.EmailAlreadyUsedException;
import com.github.subskeeper.user.exceptions.InvalidUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.github.subskeeper.utils.JwtUtils;

import java.util.Set;

@Service
public class UserService {
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserService(
            UserRepository repository
    ) {
        userRepository = repository;
    }

//    Authority roleUser = authorityRepository.findByName("ROLE_USER").get();
//    Authority roleUser = new Authority("ROLE_USER");

    public String createUser(String email, String rawPassword) {
        if (userRepository.existsByEmail(email)) throw new EmailAlreadyUsedException("This email is already used");
        String encodedPassword = encoder.encode(rawPassword);

        User predefinedUser = new User(email, encodedPassword);
        predefinedUser.setRoles(Set.of(authorityRepository.findByName("ROLE_USER").get()));
        predefinedUser.setEnabled(true);
        User savedUser = userRepository.save(predefinedUser);
        return jwtUtils.generateToken(savedUser);
    }

    public String authenticate(String email, String rawPassword) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new InvalidUserException("Password or email incorrect"));
        if (encoder.matches(rawPassword, user.getPassword())) {
            return jwtUtils.generateToken(user);
        } else {
            throw new InvalidUserException("Password or email incorrect");
        }
    }
}
