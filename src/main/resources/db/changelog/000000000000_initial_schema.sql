--liquibase formatted

--changeset seseque:initial_schema
create sequence if not exists sequence_generator start 1;

create table if not exists "users"
(
    "id"                int8 not null,
    "email"             varchar(255),
    "password_hash"     varchar(255),
    "enabled"           boolean not null,
    primary key (id)
);

create table if not exists "authorities"
(
    "id"                int8 not null,
    "name"              varchar(50),
    primary key (id)
);

create table if not exists "user_authorities"
(
    "user_id"       int8 not null references users (id),
    "role_id"       int8 not null references authorities (id)
);

insert into authorities values (1, 'ROLE_USER'), (2, 'ROLE_ADMIN');

create sequence if not exists authorities_sequence start 3;